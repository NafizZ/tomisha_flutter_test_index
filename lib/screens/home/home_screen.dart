import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../components/background.dart';
import '../../components/tabs.dart';
import '../../constants.dart';
import '../../responsive.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late ScrollController controller;

  @override
  void initState() {
    super.initState();

    controller = ScrollController();
  }

  @override
  void dispose() {
    controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Responsive(
        mobile: Scaffold(
          appBar:  PreferredSize(
            preferredSize: const Size.fromHeight(67),
            child: Column(
              children: [
                Container(
                  height: 5,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.bottomRight,
                      end: Alignment.bottomLeft,
                      colors: [
                        kprimaryColor,
                        kprimaryGradiant,
                      ],
                    )
                  ),
                ),
      
                AppBar(
                  elevation: 4,
                  shadowColor: shedoColor,
                  toolbarHeight: 62,
                  backgroundColor: Colors.white,
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(9), bottomRight: Radius.circular(9))
                  ),
                  actions: [
                    Padding(
                      padding: const EdgeInsets.only(top: 0, left: 0, bottom: 0),
                      child: TextButton(
                        onPressed: (){},
                        child: const Text(
                          "Login",
                          style: TextStyle(fontSize: 14, color: kprimaryGradiant, fontWeight: FontWeight.w500,),
                        ),
                      ),
                    ),
                  ],
                ),
              ]
            ),
          ),
          
          body: Container(
            color: Colors.white,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Background(
                    child: ListView(
                      controller: controller,
                      shrinkWrap : true,
                      primary: false,
                      physics: const BouncingScrollPhysics(),
                      // mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Padding(
                          padding: EdgeInsets.only(top: 15, right: 20, left: 20),
                          child: Text(
                            "Deine Job \n website",
                            style: TextStyle(
                              fontSize: 42,
                              fontWeight: FontWeight.w500,
                              color: textColor,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        SvgPicture.asset(
                          "assets/icons/undraw_agreement_aajr.svg",
                          fit: BoxFit.cover,
                        ),
                      ], 
                    ),
                  ),
                  const Tabs(),
                ],
              ),
            ),
          ),
          
          bottomNavigationBar: Container( 
            height: 100,
            padding: const EdgeInsets.only(top: 20, bottom: 20, left: 20, right: 20),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: const BorderRadius.only(
                topRight: Radius.circular(16),
                topLeft: Radius.circular(16),
              ),
              border: Border.all(
                width: 2,
                color: shedoColor,
                style: BorderStyle.solid,
              ),
            ),
            width: double.infinity,
            child: Container(
              decoration: BoxDecoration(
                gradient: const LinearGradient(
                  begin: Alignment.bottomRight,
                  end: Alignment.bottomLeft,
                  colors: [
                    kprimaryColor,
                    kprimaryGradiant,
                  ],
                ),
                borderRadius: BorderRadius.circular(10),
              ),
              child: TextButton(
                onPressed: () {  },
                child: const Text(
                  "Kostenlos Registrieren",
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          
        ), 
        desktop: Scaffold(
          appBar: PreferredSize(
            preferredSize: const Size.fromHeight(67),
            child: Column(
              children: [
                Container(
                  height: 5,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.bottomRight,
                      end: Alignment.bottomLeft,
                      colors: [
                        kprimaryColor,
                        kprimaryGradiant,
                      ],
                    )
                  ),
                ),
      
                AppBar(
                  elevation: 4,
                  shadowColor: shedoColor,
                  toolbarHeight: 62,
                  backgroundColor: Colors.white,
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16), bottomRight: Radius.circular(16))
                  ),
                  actions: [
                    Padding(
                      padding: const EdgeInsets.only(right: 60),
                      child: TextButton(
                        onPressed: (){},
                        child: const Text(
                          "Login",
                          style: TextStyle(fontSize: 18, color: kprimaryGradiant, fontWeight: FontWeight.w500,),
                        ),
                      ),
                    ),
                  ],
                ),
              ]
            ),
          ),
          body: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: ListView(
              shrinkWrap: true,
              children: [
                Background(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 50),
                    child: SizedBox(
                      width: 1000,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Flexible(
                            flex: 2,
                            child: Column(
                              // mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Padding(
                                  padding: EdgeInsets.only(top: 15, right: 20, left: 20, bottom: 60),
                                  child: Text(
                                    "Deine Job \n website",
                                    style: TextStyle(
                                      fontSize: 65,
                                      fontWeight: FontWeight.w500,
                                      color: textColor,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                Container(
                                  height: 40,
                                  width: 320,
                                  decoration: BoxDecoration(
                                    gradient: const LinearGradient(
                                      begin: Alignment.bottomRight,
                                      end: Alignment.bottomLeft,
                                      colors: [
                                        kprimaryColor,
                                        kprimaryGradiant,
                                      ],
                                    ),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: TextButton(
                                    onPressed: () {  },
                                    child: const Text(
                                      "Kostenlos Registrieren",
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Flexible(
                            flex: 1,
                            child: Center(
                              child: CircleAvatar(
                                backgroundColor: Colors.white,
                                radius: 200,
                                child: ClipOval(
                                  child: SvgPicture.asset(
                                    "assets/icons/undraw_agreement_aajr.svg",
                                    fit: BoxFit.cover,
                                  ),
                                ), //Text
                              ), //CircleAvatar
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                const Tabs(),
              ],
            ),
          ),
        ),
        
      )
      
    );
  }
}