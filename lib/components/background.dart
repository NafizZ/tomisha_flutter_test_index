import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Background extends StatelessWidget {
  final Widget child;
  const Background({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Size _size = MediaQuery.of(context).size;
    return Container(
      // width: double.infinity,
      // height: MediaQuery.of(context).size.height,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Center(
            child: SvgPicture.asset(
              "assets/images/background.svg",
              fit: BoxFit.fitWidth,
              // width: _size.width,
              // height: _size.height,
            ),
          ),
          child,
        ],
      ),
    );
  }
}
