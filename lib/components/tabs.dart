import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:tomisha_flutter_test_index/components/background.dart';

import '../constants.dart';

double responsiveValue = 0;
class Tabs extends StatelessWidget {
  const Tabs({super.key});

  @override
  Widget build(BuildContext context) {
    Size _size = MediaQuery.of(context).size;
    if(_size.width > 900){
      responsiveValue = 0.5;
    }
    else{
      responsiveValue = 0;
    }
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: DefaultTabController(
          initialIndex: 0,
          length: 3,
          child: Column(
            children: <Widget>[
              Container(
                height: 45,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0),
                  border: const Border.fromBorderSide(
                    BorderSide(
                      width: 1,
                      color: tabBarBorderColor,
                      // style: BorderStyle.solid
                    ),//BorderSide
                  ), 
                ),
                margin: EdgeInsets.symmetric(horizontal: _size.width > 900 ? 350 : 30),
                child: TabBar(
                    indicator: BoxDecoration(
                      color: selectedTabColor,
                      borderRadius:  BorderRadius.circular(10.0),
                    ) ,
                    labelColor: Colors.white,
                    unselectedLabelColor: tabBarTextColor,
                    labelStyle: const TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 12,
                    ),
                    tabs: const <Widget>[
                      SizedBox(
                        width: 140,
                        child: Center(
                          child: Text("Arbeitnehmer"),
                        ),
                      ),
                      
                      SizedBox(
                        width: 140,
                        child: Center(
                          child: Text("Arbeitgeber"),
                        ),
                      ),
                      SizedBox(
                        width: 140,
                        child: Center(
                          child: Text("Temporärbüro"),
                        ),
                      ),
                    ]),
              ),
            //using expanded means you dont have set height manually
            Container(
              margin: const EdgeInsets.symmetric(vertical: 25),
              padding: const EdgeInsets.symmetric(horizontal: 0),
              height: _size.width > 900 ? 1800 : 1200,
              child: TabBarView(
                physics: const BouncingScrollPhysics(),
                children: <Widget>[
                  Column(
                    children: [
                      //first tab start
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
                        child: Text(
                          "Drei einfache Schritte\n zu deinem neuen Job",
                          style: TextStyle(
                            fontSize: _size.width > 900 ? 34 :24,
                            fontWeight: FontWeight.w500,
                            color: titleUnderTabBarColor,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(
                        // padding: EdgeInsets.symmetric(vertical: 20),
                        height: 300 + 300*responsiveValue,
                        width: 254+ 254*responsiveValue,
                        child: Stack(
                          children: [
                            Positioned(
                              right: 0,
                              top: 0,
                              child: SizedBox(
                                height: 145+ 145*responsiveValue,
                                width: 220 + 220*responsiveValue,
                                child: SvgPicture.asset(
                                  "assets/icons/undraw_Profile_data_re_v81r.svg",
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            Positioned(
                              bottom: 40,
                              left: 0,
                              child: SizedBox(
                                height: 160 + 160*responsiveValue,
                                width: 110 +  110*responsiveValue,
                                child: Text(
                                  "1.",
                                  style: TextStyle(
                                    fontSize: 120 + 120*responsiveValue,
                                    fontWeight: FontWeight.w500,
                                    color: numberUnderTabBarColor,
                                    letterSpacing: -20,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            Positioned(
                              bottom: 55,
                              right: 0,
                              child: SizedBox(
                                height: 50 + 50*responsiveValue,
                                width: 200 + 200*responsiveValue,
                                child: Text(
                                  "Erstellen dein Lebenslauf",
                                  style: TextStyle(
                                    fontSize: 16 + 16*responsiveValue,
                                    fontWeight: FontWeight.w500,
                                    color: numberUnderTabBarColor,
                                    letterSpacing: 0,
                                  ),
                                  textAlign: TextAlign.right,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Background(
                        child: SizedBox(
                          // padding: EdgeInsets.symmetric(vertical: 20),
                          height: 300 + 300*responsiveValue,
                          width: 254 + 254*responsiveValue,
                          child: Stack(
                            children: [
                              Positioned(
                                top: 0,
                                left: 0,
                                child: SizedBox(
                                  height: 160 + 160*responsiveValue,
                                  width: 110 + 110*responsiveValue,
                                  child: Text(
                                    "2.",
                                    style: TextStyle(
                                      fontSize: 112 + 112*responsiveValue,
                                      fontWeight: FontWeight.w500,
                                      color: numberUnderTabBarColor,
                                      letterSpacing: -10,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                              Positioned(
                                top: 90,
                                right: 0,

                                child: SizedBox(
                                  height: 50 + 50*responsiveValue,
                                  width: 200 + 200*responsiveValue,
                                  child: Text(
                                    "Erstellen dein Lebenslauf",
                                    style: TextStyle(
                                      fontSize: 16 + 16*responsiveValue,
                                      fontWeight: FontWeight.w500,
                                      color: numberUnderTabBarColor,
                                      letterSpacing: 0,
                                    ),
                                    textAlign: TextAlign.right,
                                  ),
                                ),
                              ),
                              Positioned(
                                bottom: 0,
                                left: 0,
                                child: Container(
                                  height: 126+ 126*responsiveValue,
                                  width: 180 + 180*responsiveValue,
                                  alignment: Alignment.topRight,
                                  child: SvgPicture.asset(
                                    "assets/icons/undraw_task_31wc.svg",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      
                      Container(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        height: 300 + 300*responsiveValue,
                        width: 254 + 254*responsiveValue,
                        child: Stack(
                          children: [
                            Positioned(
                              top: 0,
                              left: 0,
                              child: SizedBox(
                                height: 160 + 160*responsiveValue,
                                width: 110 + 110*responsiveValue,
                                child: Text(
                                  "3.",
                                  style: TextStyle(
                                    fontSize: 112 + 112*responsiveValue,
                                    fontWeight: FontWeight.w500,
                                    color: numberUnderTabBarColor,
                                    letterSpacing: -10,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            Positioned(
                              top: 50,
                              right: 0,
                              left: 110,
                              child: Container(
                                height: 50 + 50*responsiveValue,
                                width: 200 + 200*responsiveValue,
                                alignment: Alignment.bottomRight,
                                child: Text(
                                  "Mit nur einem Klick bewerben",
                                  style: TextStyle(
                                    fontSize: 16+ 16*responsiveValue,
                                    fontWeight: FontWeight.w500,
                                    color: numberUnderTabBarColor,
                                    letterSpacing: 0,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            Positioned(
                              bottom: 0,
                              left: 0,
                              child: Container(
                                alignment: Alignment.bottomRight,
                                height: 126 + 126*responsiveValue,
                                width: 180 + 180*responsiveValue,
                                child: SvgPicture.asset(
                                  "assets/icons/undraw_personal_file_222m.svg",
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  // first tab end
                  //second tab start here
                  Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
                        child: Text(
                          "Drei einfache Schritte\nzu deinem neuen Mitarbeiter",
                          style: TextStyle(
                            fontSize: _size.width > 900 ? 34 :24,
                            fontWeight: FontWeight.w500,
                            color: titleUnderTabBarColor,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(
                        // padding: EdgeInsets.symmetric(vertical: 20),
                        height: 300,
                        width: 254,
                        child: Stack(
                          children: [
                            Positioned(
                              right: 0,
                              top: 0,
                              child: SizedBox(
                                height: 145,
                                width: 220,
                                child: SvgPicture.asset(
                                  "assets/icons/undraw_Profile_data_re_v81r.svg",
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            const Positioned(
                              bottom: 40,
                              left: 0,
                              child: SizedBox(
                                height: 160,
                                width: 110,
                                child: Text(
                                  "1.",
                                  style: TextStyle(
                                    fontSize: 120,
                                    fontWeight: FontWeight.w500,
                                    color: numberUnderTabBarColor,
                                    letterSpacing: -20,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            const Positioned(
                              bottom: 75,
                              right: 0,
                              left: 100,
                              child: SizedBox(
                                height: 50,
                                width: 200,
                                child: Text(
                                  "Erstellen dein Unternehmensprofil",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: numberUnderTabBarColor,
                                    letterSpacing: 0,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Background(
                        child: SizedBox(
                          // padding: EdgeInsets.symmetric(vertical: 20),
                          height: 300,
                          width: 254,
                          child: Stack(
                            children: [
                              const Positioned(
                                top: 0,
                                left: 0,
                                child: SizedBox(
                                  height: 160,
                                  width: 110,
                                  child: Text(
                                    "2.",
                                    style: TextStyle(
                                      fontSize: 112,
                                      fontWeight: FontWeight.w500,
                                      color: numberUnderTabBarColor,
                                      letterSpacing: -10,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                              const Positioned(
                                top: 90,
                                right: 0,
                      
                                child: SizedBox(
                                  height: 50,
                                  width: 200,
                                  child: Text(
                                    "Erstellen ein Jobinserat",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      color: numberUnderTabBarColor,
                                      letterSpacing: 0,
                                    ),
                                    textAlign: TextAlign.right,
                                  ),
                                ),
                              ),
                              Positioned(
                                bottom: 0,
                                left: 0,
                                child: Container(
                                  height: 180,
                                  width: 258,
                                  alignment: Alignment.topRight,
                                  child: SvgPicture.asset(
                                    "assets/icons/undraw_about_me_wa29.svg",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        height: 300,
                        width: 254,
                        child: Stack(
                          children: [
                            const Positioned(
                              top: 0,
                              left: 0,
                              child: SizedBox(
                                height: 160,
                                width: 110,
                                child: Text(
                                  "3.",
                                  style: TextStyle(
                                    fontSize: 112,
                                    fontWeight: FontWeight.w500,
                                    color: numberUnderTabBarColor,
                                    letterSpacing: -10,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            Positioned(
                              top: 50,
                              right: 0,
                              left: 110,
                              child: Container(
                                height: 50,
                                width: 200,
                                alignment: Alignment.bottomRight,
                                child: const Text(
                                  "Wähle deinen neuen Mitarbeiter aus",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: numberUnderTabBarColor,
                                    letterSpacing: 0,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            Positioned(
                              bottom: 0,
                              left: 0,
                              child: Container(
                                alignment: Alignment.bottomRight,
                                height: 140,
                                width: 200,
                                child: SvgPicture.asset(
                                  "assets/icons/undraw_swipe_profiles1_i6mr.svg",
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  //second tab end here
                  Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 20),
                        child: Text(
                          "Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter",
                          style: TextStyle(
                            fontSize: _size.width > 900 ? 34 :23,
                            fontWeight: FontWeight.w500,
                            color: titleUnderTabBarColor,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                       SizedBox(
                        // padding: EdgeInsets.symmetric(vertical: 20),
                        height: 300,
                        width: 254,
                        child: Stack(
                          children: [
                            Positioned(
                              right: 0,
                              top: 0,
                              child: SizedBox(
                                height: 145,
                                width: 220,
                                child: SvgPicture.asset(
                                  "assets/icons/undraw_Profile_data_re_v81r.svg",
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                            const Positioned(
                              bottom: 40,
                              left: 0,
                              child: SizedBox(
                                height: 160,
                                width: 110,
                                child: Text(
                                  "1.",
                                  style: TextStyle(
                                    fontSize: 120,
                                    fontWeight: FontWeight.w500,
                                    color: numberUnderTabBarColor,
                                    letterSpacing: -20,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            const Positioned(
                              bottom: 75,
                              right: 0,
                              left: 100,
                              child: SizedBox(
                                height: 50,
                                width: 200,
                                child: Text(
                                  "Erstellen dein Unternehmensprofil",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: numberUnderTabBarColor,
                                    letterSpacing: 0,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Background(
                        child: SizedBox(
                          // padding: EdgeInsets.symmetric(vertical: 20),
                          height: 300,
                          width: 254,
                          child: Stack(
                            children: [
                              const Positioned(
                                top: 0,
                                left: 0,
                                child: SizedBox(
                                  height: 160,
                                  width: 110,
                                  child: Text(
                                    "2.",
                                    style: TextStyle(
                                      fontSize: 112,
                                      fontWeight: FontWeight.w500,
                                      color: numberUnderTabBarColor,
                                      letterSpacing: -10,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                              const Positioned(
                                top: 45,
                                right: 0,
                                left: 90,
                                child: SizedBox(
                                  height: 80,
                                  width: 220,
                                  child: Text(
                                    "Erhalte Vermittlungs- angebot von Arbeitgeber",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      color: numberUnderTabBarColor,
                                      letterSpacing: 0,
                                    ),
                                    textAlign: TextAlign.left,
                                  ),
                                ),
                              ),
                              Positioned(
                                bottom: 0,
                                left: 0,
                                child: Container(
                                  height: 180,
                                  width: 258,
                                  alignment: Alignment.topRight,
                                  child: SvgPicture.asset(
                                    "assets/icons/undraw_job_offers_kw5d.svg",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        height: 300,
                        width: 254,
                        child: Stack(
                          children: [
                            const Positioned(
                              top: 0,
                              left: 0,
                              child: SizedBox(
                                height: 160,
                                width: 110,
                                child: Text(
                                  "3.",
                                  style: TextStyle(
                                    fontSize: 112,
                                    fontWeight: FontWeight.w500,
                                    color: numberUnderTabBarColor,
                                    letterSpacing: -10,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            Positioned(
                              top: 20,
                              right: 0,
                              left: 110,
                              child: Container(
                                height: 80,
                                width: 200,
                                alignment: Alignment.bottomRight,
                                child: const Text(
                                  "Vermittlung nach Provision oder Stundenlohn",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: numberUnderTabBarColor,
                                    letterSpacing: 0,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                            Positioned(
                              bottom: 0,
                              left: 0,
                              child: Container(
                                alignment: Alignment.bottomRight,
                                height: 140,
                                width: 200,
                                child: SvgPicture.asset(
                                  "assets/icons/undraw_business_deal_cpi9.svg",
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  )
                ]
              ),
            ),
            ],
          ),
      ),
    );
  }
}