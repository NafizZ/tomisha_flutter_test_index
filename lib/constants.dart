import 'package:flutter/material.dart';

const kprimaryColor = Color.fromRGBO(49, 130, 206, 1);
const kprimaryGradiant = Color.fromRGBO(49, 151, 149, 1);
const shedoColor = Color.fromRGBO(214, 214, 214, 1);
const backgroudColor = Color.fromRGBO(230,255,250, 1);
const textColor = Color.fromRGBO(45,55,72, 1);
const secondaryBackgroudColor = Color.fromRGBO(235,244,255, 1);
const selectedTabColor = Color.fromRGBO(129, 230, 217, 1);
const tabBarTextColor = Color.fromRGBO(49, 151, 149, 1);
const tabBarBorderColor = Color.fromRGBO(203, 213, 224, 1);
const titleUnderTabBarColor = Color.fromRGBO(74, 85, 104, 1);
const numberUnderTabBarColor = Color.fromRGBO(113, 128, 150, 1);

